#got into the folder

#download FlickrDataset 8k

#download Flickrtextset 8k

#if you want to use VGG16 mode us the following cmd format

# !python3 VGG16.py image-path

#if you want to use Resnet50 mode us the following cmd format

# !python3 Resnet50.py image-path

#if you want to use InceptionV3 mode us the following cmd format

# !python3 InceptionV3.py image-path

#example

#!python3 VGG16.py '/content/drive/MyDrive/dogs.jpg'

#!python3 Resnet50.py '/content/drive/MyDrive/dogs.jpg'

#!python3 InceptionV3.py '/content/drive/MyDrive/dogs.jpg'
