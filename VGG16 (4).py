import numpy as np
from numpy import array
import pandas as pd
import matplotlib.pyplot as plt
import keras
import re
import nltk
from nltk.corpus import words
import string
import json
import keras
from time import time
from pickle import dump,load
from keras.applications.vgg16 import VGG16 #pre trained model for CNN
from keras.applications.resnet50 import ResNet50,preprocess_input,decode_predictions#pre trained model for RNN
from keras.preprocessing import image
from keras.models import Model,load_model
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Input,Dense,Dropout,Embedding,LSTM
import os
import sys
from keras.layers.merge import add
import collections
import pickle
#cleaning the text
#lower the text
#remove punctuations
#remove words less than length 1
def clean_text(sample):
  sample=sample.lower()
  sample=re.sub("[^a-z]+"," ",sample) 
  sample=sample.split()
  sample=[s for s in sample if len(s)>1]
  sample=" ".join(sample)
  return sample
def preprocess_image(image_path):
  img=image.load_img(image_path,target_size=(224,224))
  img=image.img_to_array(img)
  img=np.expand_dims(img,axis=0)
  img=preprocess_input(img)
  return img
def encode_image(img):
  img=preprocess_image(img)
  feature_vector=model_new.predict(img)
  feature_vector=feature_vector.reshape(feature_vector.shape[1],)
  return feature_vector
def encodingimage(data,images,imgpath):
  encoding_data={}
  for ix,img in enumerate(data):
    img=imgpath+"{}.jpg".format(data[ix])
    encoding_data[img[len(images):]]=encode_image(img)
    if(ix%100==0):
      print("Encoding image "+str(ix))
  return encoding_data
def wrdidx_idxwrd(all_vocab):
  ix=1
  word_to_idx={}
  idx_to_word={}
  for e in all_vocab:
    word_to_idx[e]=ix
    idx_to_word[ix]=e
    ix=ix+1
  return word_to_idx,idx_to_word
def get_embedding_output():
  emb_dim=50
  embedding_output=np.zeros((vocab_size,emb_dim))
  for word,idx in word_to_idx.items():
    embedding_vector=embedding_index.get(word)
    if embedding_vector is not None:
      embedding_output[idx]=embedding_vector
  return embedding_output
def predict_caption(photo):
  in_text = 'startseq'
  for i in range(max_len):
    sequence = [word_to_idx[w] for w in in_text.split() if w in word_to_idx]
    sequence = pad_sequences([sequence], maxlen=max_len,padding='post')
    ypred = model2.predict([photo,sequence], verbose=0)
    ypred = np.argmax(ypred)
    word = idx_to_word[ypred]
    in_text += ' ' + word
    if word == 'endseq':
      break
  final_caption = in_text.split()
  final_caption = final_caption[1:-1]
  final_caption = ' '.join(final_caption)
  return final_caption
if __name__ == '__main__':
  if(len(sys.argv)<1):
    print("Invalid")
    sys.exit()
  else:
    filename='Flickrtext/Flickr8k.token.txt'
    with open(filename) as filepath:
      captions=filepath.read()
      filepath.close()
    captions_lines=captions.split("\n")[:-1]
    descriptions={}
    for ele in captions_lines:
      imagename=ele.split("\t")[0]
      imagedesc=ele.split("\t")[1]
      imagename=imagename.split(".")[0]
      if imagename not in descriptions:
        descriptions[imagename]=list()
      descriptions[imagename].append(imagedesc)
    for key,desc_list in descriptions.items():
      for i in range(0,len(desc_list)):
        desc_list[i]=clean_text(desc_list[i])
    f=open("descriptions_captions_VGG16.txt","w")
    f.write(str(descriptions))
    f.close()
    vocabulary=set()
    for key in descriptions.keys():
      [vocabulary.update(i.split()) for i in descriptions[key]]
    all_vocab=[]
    for key in descriptions.keys():
      [all_vocab.append(i) for des in descriptions[key] for i in des.split()]
    counter=collections.Counter(all_vocab)
    dic_=dict(counter)
    threshold_val=10
    sorted_dic=sorted(dic_.items(),reverse=True,key=lambda x:x[1])
    sorted_dic=[x for x in sorted_dic if x[1]>threshold_val]
    all_vocab=[x[0] for x in sorted_dic]
    f=open('Flickrtext/Flickr_8k.trainImages.txt')
    train=f.read()
    f.close()
    train=[e.split(".")[0] for e in train.split("\n")[:-1]]
    f=open('Flickrtext/Flickr_8k.testImages.txt')
    test=f.read()
    f.close()
    test=[e.split(".")[0] for e in test.split("\n")[:-1]]
    train_descriptions={}
    for t in train:
      train_descriptions[t]=[]
      for cap in descriptions[t]:
        cap_to_append="startseq "+cap+" endseq"
        train_descriptions[t].append(cap_to_append)
    model=VGG16(weights="imagenet")
    model_new=Model(model.input,model.layers[-2].output)
    images = 'Flicker8k_Dataset'
    imgpath='Flicker8k_Dataset/'
    encoding_train=encodingimage(train,images,imgpath)#run only once
    encoding_test=encodingimage(test,images,imgpath)#run only once
    #now modify the the encoding test and training sets
    #run only once
    encoding_train_mod={}
    encoding_test_mod={}
    for key in encoding_train.keys():
      lister=key.split("/")
      encoding_train_mod[lister[1]]=encoding_train[key]
    for key in encoding_test.keys():
      lister=key.split("/")
      encoding_test_mod[lister[1]]=encoding_test[key]
    with open('encoded_train_images_save_vgg16.pkl','wb') as encoded_pickle:
      pickle.dump(encoding_train_mod,encoded_pickle)#run only once
    with open('encoded_test_images_save_vgg16.pkl','wb') as encoded_pickle:
      pickle.dump(encoding_test_mod,encoded_pickle)#run only once
    train_features=load(open('encoded_train_images_save_vgg16.pkl','rb'))
    test_features=load(open('encoded_test_images_save_vgg16.pkl','rb'))
    word_to_idx,idx_to_word=wrdidx_idxwrd(all_vocab)
    word_to_idx['startseq']=1846
    word_to_idx['endseq']=1847
    idx_to_word[1846]='startseq'
    idx_to_word[1847]='endseq'
    #append 0's as well
    all_caption_len=[]
    vocab_size=len(idx_to_word)+1
    for key in train_descriptions.keys():
      for cap in train_descriptions[key]:
        all_caption_len.append(len(cap.split()))
    max_len=max(all_caption_len)
    X1, X2, y = list(), list(), list()
    for key, desc_list in train_descriptions.items():
      photo = train_features[key+'.jpg']
      for desc in desc_list:
        seq = [word_to_idx[word] for word in desc.split() if word in word_to_idx]
        for i in range(1, len(seq)):
          in_seq=seq[0:i]
          out_seq=seq[i]
          in_seq=pad_sequences([in_seq],maxlen=max_len,value=0,padding='post')[0]
          out_seq=to_categorical([out_seq],num_classes=vocab_size)[0]
          X1.append(photo)
          X2.append(in_seq)
          y.append(out_seq)
    X2=np.array(X2)
    X1=np.array(X1)
    y=np.array(y)
    f=open('glove.6B.50d.txt',encoding='utf-8')
    embedding_index={}
    for line in f:
      values=line.split()
      word=values[0]
      coefs=np.asarray(values[1:],dtype='float')
      embedding_index[word]=coefs
    f.close()
    embedding_output=get_embedding_output()
    ip1 = Input(shape = (4096, ))
    fe1 = Dropout(0.2)(ip1)
    fe2 = Dense(256, activation = 'relu')(fe1)
    ip2 = Input(shape = (max_len, ))
    se1 = Embedding(vocab_size, 50, mask_zero = True)(ip2)
    se2 = Dropout(0.2)(se1)
    se3 = LSTM(256)(se2)
    decoder1 = add([fe2, se3])
    decoder2 = Dense(256, activation = 'relu')(decoder1)
    outputs = Dense(vocab_size, activation = 'softmax')(decoder2)
    model2 = Model(inputs = [ip1, ip2], outputs = outputs)
    model2.layers[2].set_weights([embedding_output])
    model2.layers[2].trainable=False
    model2.compile(loss="categorical_crossentropy",optimizer='adam')
    for i in range(30):
      model2.fit([X1,X2],y,epochs=1,batch_size=256)
    model2.save_weights("imagecaptionmodel_vgg16"+str(i)+".h5")
    model2.load_weights('imagecaptionmodel_vgg16'+str(i)+'.h5')
    test1=encode_image(sys.argv[1])
    photo=test1.reshape((1,4096))
    i=plt.imread(sys.argv[1])
    plt.imshow(i)
    plt.axis("off")
    plt.show()
    caption=predict_caption(photo)
    print(caption)






    











  